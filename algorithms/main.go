package solveThings

type (
	Tower struct{
		int x
		int y
		float64 R
		uint c
	}
	
	Client struct{
		int x
		int y
	}

	clientSet struct{
		Client origin
		Tower[] elements
	}
	
	towerSet struct{
		Tower origin
		Client[] elements
	}
)

func Solve(towers []Tower, clients []Client){
	towerSets, clientSets := findConnectionSets(towers, clients)
	var toDelete  []toweSet
	for toDelete != nil {
		toDelete = nil
		for _, ts := range towerSets {
			if len(ts.elements) < ts.origin.c{
				toDelete = append(toDelete, ts)
			}
		}
		var i = 0
		for _, td := range toDelete{
			for ; i < len(towerSets); i++{
				if towerSets[i] == td{
					copy(towerSets[i:], towerSets[i+1:])
					towerSets[len(towerSets)-1] = ""
					towerSets = towerSets[:len(towerSets)-1]
					break
				}
			}
		}
	}
	
	var partition [][]towerSet
	var clientMap = make(map[Client]int)
	var mergeSets
	for _, ts := range towerSets {
		foundSet := -1
		for _, c := range ts.elements {
			if s, ok := clientMap[c]; ok{
				if foundSet != 0{
					
				} 
				foundSet = s
				partition[s] = append(partition[s], ts)
			}
		}
		if !foundSet {
			partition = append(partition, []towerSet{ts})
		}
	}

	
}

func findConnectionSets(towers []Tower, clients []Client) ([]towerSet, []clientSet){

	towerSet := towerSet{
		elements: make([]Client, len(towers)),
	}
	clientSet := clientSet{
		elements: make([]Tower, len(clients)),
	}
	for ti, t := range towers {
		towerSet[ti].origin = t
		for ci, c := range clients {
			clientSet[ci].origin = c

			if (math.Sqrt(math.Pow(float64(t.x-c.x), 2.0)+math.Pow(float64(t.y-c.y)))) {
				towerSet[ti].elements = append(sets[ti].elements, c)
				clientSet[ci].elements = append(clientSet[ci].elements, t)
			}
		}
	}
	return sets
}
