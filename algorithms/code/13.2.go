package main

import (
	"fmt"
	"strconv"
)

type (
	probeField struct {
		filled bool
		tombstone bool
		key int 
		value int
	}
)

func (pf probeField) String() string {
	if pf.filled{
		return strconv.Itoa(pf.key) + "(" + strconv.Itoa(pf.value) + ")"
	} else {
		return "\\_"
	}
}

func hash(k int) int {
	return (3*k+1)%13
}

func probeDeleteLinear(table []probeField, k int){
	h := hash(k)
	i := 0
	for ; table[(h+i)%13].key != k; i++ {}
	table[(h+i)%13].filled = false
}

func probeDeleteDouble(table []probeField, k int){
	h := hash(k)
	hp := k%15
	i := 0
	for ; table[(h+i*hp)%13].key != k; i++ {}
	table[(h+i*hp)%13].filled = false
}

func probeInsertLinear(table []probeField, k int){
	h := hash(k)
	i := 0
	for ; table[(h+i)%13].filled; i++ {}
	table[(h+i)%13].filled = true
	table[(h+i)%13].tombstone = true
	table[(h+i)%13].key = k
	table[(h+i)%13].value = i+1
}

func probeInsertDouble(table []probeField, k int){
	h := hash(k)
	hp := k%15
	i := 0
	for ; table[(h+i*hp)%13].filled; i++ {}
	table[(h+i*hp)%13].filled = true
	table[(h+i*hp)%13].tombstone = true
	table[(h+i*hp)%13].key = k
	table[(h+i*hp)%13].value = i+1
}

func probeSearchLinear(table []probeField, k int){
	h := hash(k)
	i := 0
	for ; table[(h+i)%13].key != k && table[(h+i)%13].filled; i++ {}
	table[(h+i)%13].filled = false
}

func probeSearchDouble(table []probeField, k int){
	h := hash(k)
	hp := k%15
	i := 0
	for ; table[(h+i*hp)%13].key != k && table[(h+i*hp)%13].filled; i++ {}
	table[(h+i*hp)%13].filled = false
}

func main() {
	table0 := [13]probeField{}
	table1 := [13]probeField{}
	for _, v := range []int{8, 12, 40, 13, 88, 45, 29, 20, 23, 77}{
		probeInsertLinear(table0[:], v)
		probeInsertDouble(table1[:], v)
	}
	fmt.Println(table0)
	fmt.Println(table1)
}
